package com.waaangyi.leveldisplay;

import com.waaangyi.leveldisplay.api.Api;
import com.waaangyi.leveldisplay.api.Cache;
import com.waaangyi.leveldisplay.command.ApiKey;
import com.waaangyi.leveldisplay.command.Command;
import com.waaangyi.leveldisplay.config.Config;
import com.waaangyi.leveldisplay.keybinds.KeyBinds;
import com.waaangyi.leveldisplay.keybinds.KeybindingEventHandler;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.io.File;


@Mod(modid = LevelDisplay.MODID, version = LevelDisplay.VERSION, clientSideOnly = true)
public class LevelDisplay {
    public static final String MODID = "leveldisplay";
    public static final String VERSION = "1.0";
    private static Config config;
    private static Api api;
    private static Cache cache;

    public static String getApiKey() {
        return config.getApiKey();
    }

    public static void setApiKey(String apiKey) {
        config.setApiKey(apiKey);
    }

    public static Cache getCache() {
        return cache;
    }

    public static Config getConfig() {
        return config;
    }

    public static Api getApi() {
        return api;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        File configFile = event.getSuggestedConfigurationFile();
        config = new Config(configFile);
        config.loadConfig();

        MinecraftForge.EVENT_BUS.register(new KeybindingEventHandler());
        //MinecraftForge.EVENT_BUS.register(new Cache());

        new KeyBinds();

        String diskCache = event.getModConfigurationDirectory().getAbsolutePath();
        cache = new Cache(new File(diskCache + "/LevelDisplayCache.json"));
        MinecraftForge.EVENT_BUS.register(cache);

        api = new Api();


    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        ClientCommandHandler.instance.registerCommand(new Command());
        ClientCommandHandler.instance.registerCommand(new ApiKey());

    }


}
