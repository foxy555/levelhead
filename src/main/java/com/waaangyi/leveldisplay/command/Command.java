package com.waaangyi.leveldisplay.command;

import com.waaangyi.leveldisplay.level.Level;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;


public class Command extends CommandBase {
    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }

    @Override
    public String getCommandName() {
        return "getlevel";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "";
    }


    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        Runnable r = new Level(sender);
        new Thread(r).start();
    }


}
