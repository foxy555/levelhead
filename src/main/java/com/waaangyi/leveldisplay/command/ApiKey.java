package com.waaangyi.leveldisplay.command;

import com.waaangyi.leveldisplay.LevelDisplay;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class ApiKey extends CommandBase {
    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }

    @Override
    public String getCommandName() {
        return "apikey";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "apikey set - set the apikey\napikey - print the apikey";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 2) {
            if (args[0].equals("set")) {
                LevelDisplay.setApiKey(args[1]);
            } else {
                sender.addChatMessage(new ChatComponentText(getCommandUsage(sender)));
            }
        } else {
            sender.addChatMessage(new ChatComponentText(LevelDisplay.getApiKey()));
        }
    }


}
