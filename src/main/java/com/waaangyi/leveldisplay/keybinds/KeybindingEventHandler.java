package com.waaangyi.leveldisplay.keybinds;

import com.waaangyi.leveldisplay.level.Level;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class KeybindingEventHandler {
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
        if (KeyBinds.getLevelKey.isPressed()) {
            Runnable r = new Level();
            new Thread(r).start();
        }
    }
}
