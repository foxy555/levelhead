package com.waaangyi.leveldisplay.keybinds;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import org.lwjgl.input.Keyboard;

public class KeyBinds {
    public static KeyBinding getLevelKey;

    public KeyBinds() {
        getLevelKey = new KeyBinding("Get Level", Keyboard.KEY_R, "Waaangyi's LevelDisplay Mod");
        ClientRegistry.registerKeyBinding(getLevelKey);
    }
}
