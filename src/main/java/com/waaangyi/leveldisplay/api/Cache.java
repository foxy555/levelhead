package com.waaangyi.leveldisplay.api;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waaangyi.leveldisplay.var.Pair;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Cache {
    private Map<UUID, Pair<Integer, Long>> cache;
    private long lifetime;
    private File diskCache;
    private long lastSave;

    public Cache(File diskCache) {
        System.out.println("Cache initiated");
        this.diskCache = diskCache;
        System.out.println("Location: " + this.diskCache);
        Map<UUID, Pair<Integer, Long>> c = loadCache();
        if (c == null)
            this.cache = new HashMap<UUID, Pair<Integer, Long>>();
        else
            this.cache = c;
        this.lifetime = 86400000 * 2;
        this.lastSave = 0;
    }

    private Map<UUID, Pair<Integer, Long>> loadCache() {
        Type type = new TypeToken<HashMap<UUID, Pair<Integer, Long>>>() {
        }.getType();
        Gson gson = new Gson();
        try {
            String json = FileUtils.readFileToString(this.diskCache);
            return gson.fromJson(json, type);
        } catch (FileNotFoundException e) {
            System.out.println("Cache not found, creating...");
        } catch (IOException e) {
            System.out.println("Cache cannot be read");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int get(UUID key) {
        if (this.cache.containsKey(key)) {
            Pair<Integer, Long> pair = this.cache.get(key);
            if ((System.currentTimeMillis() - pair.getValue()) < this.lifetime) {
                return pair.getKey();
            } else {
                return 0;
            }
        }
        // Assume the Player does not exist, the cache will be updated later if otherwise
        set(key, -1);
        return 0;
    }

    public int get(UUID key, boolean override) {
        if (override)
            if (this.cache.containsKey(key))
                return this.cache.get(key).getKey();
        return 0;
    }

    public void set(UUID uuid, int level) {
        this.cache.put(uuid, new Pair<Integer, Long>(level, System.currentTimeMillis()));
    }

    @SubscribeEvent
    public void save(WorldEvent.Unload event) {
        if (System.currentTimeMillis() - this.lastSave < 3000)
            return;
        System.out.println("Time to save!");
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(this.cache);
        try {
            FileUtils.writeStringToFile(this.diskCache, json);
            this.lastSave = System.currentTimeMillis();
        } catch (IOException e) {
            System.out.println("Could not write to file");
        }
    }
}
