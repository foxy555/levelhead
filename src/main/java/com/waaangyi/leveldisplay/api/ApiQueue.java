package com.waaangyi.leveldisplay.api;

import java.util.LinkedList;
import java.util.Queue;

public class ApiQueue {
    private Queue<Long> queue;
    private int threshold;

    public ApiQueue() {
        this.queue = new LinkedList<Long>();
        this.threshold = 100;
    }

    public ApiQueue(int threshold) {
        this.queue = new LinkedList<Long>();
        this.threshold = threshold;
    }

    private void update() {
        long currentTime = System.currentTimeMillis();
        while (queue.size() != 0) {
            if (currentTime - this.queue.peek() >= 60000)
                this.queue.remove();
            else
                break;
        }
    }

    public boolean check() {
        update();
        return this.queue.size() < this.threshold;
    }

    public int getAvailableCount() {
        update();
        return this.threshold - this.queue.size();
    }

    public void add() {
        this.queue.add(System.currentTimeMillis());
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

}
